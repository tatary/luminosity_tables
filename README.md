# luminosity_tables

Contains the integrated number of ionization photons per solar mass. 
These tables are for Chabrier IMF. 
The subscripts [00, 01, 02, 03, 04, 05, 06, 07] correspond to the 
metallicities (Mz/M) [0.0, 1.e-4, 3.e-4, 1.e-2, 3.e-2, 1.e-2, 3.e-2, 1.e-1]. 
